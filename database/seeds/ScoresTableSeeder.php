<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class ScoresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = Faker::create();
        foreach (range(1,100) as $i) {
            DB::table("scores")->insert([
                "name" => $faker->name,
                "score_difficulty_id" => rand(1, 3),
                "score" => rand(1, 10000),
                "approved" => rand(0,1),
            ]);
        }
    }
}
