<?php

use Illuminate\Database\Seeder;

class ScoreDifficultyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        \DB::table("score_difficulties")->insert([
            ["name"=>"Easy","created_at"=>\Carbon\Carbon::now(),"updated_at"=>\Carbon\Carbon::now()],
            ["name"=>"Medium","created_at"=>\Carbon\Carbon::now(),"updated_at"=>\Carbon\Carbon::now()],
            ["name"=>"Hard","created_at"=>\Carbon\Carbon::now(),"updated_at"=>\Carbon\Carbon::now()]
        ]);
    }
}
