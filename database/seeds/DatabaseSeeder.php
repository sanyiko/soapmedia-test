<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(DemoUserSeeder::class);
        $this->call(ScoreDifficultyTableSeeder::class);
        $this->call(ScoresTableSeeder::class);
    }
}
