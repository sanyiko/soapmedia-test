<?php

use Illuminate\Database\Seeder;


class DemoUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('users')->insert([
            'name' => "Demo User",
            'email' => "example@demo.com",
            'password' => bcrypt("demo"),
        ]);
    }
}
