<?php

Route::get("/",["as"=>"index","uses"=>"FrontendController@index"]);

Route::group(["prefix" =>"scores"], function () {
    Route::get("/", ["as" => "score.list", "uses" => "FrontendController@scoreList"]);
    Route::post("/store",["as" => "score.store", "uses" => "FrontendController@scoreStore"]);
    Route::group(["prefix"=>"admin"], function () {
        Route::get("/login", ["as" => "login", "uses" => "Auth\LoginController@showLoginForm"]);
        Route::post("/login", ["as" => "auth", "uses" => "Auth\LoginController@auth"]);
        Route::group(["middleware" =>"auth"], function () {
            Route::get("/", ["as" => "admin", "uses" => "AdminController@index"]);
            Route::get("/logout", ["as" => "logout", "uses" => "AdminController@logout"]);

            Route::get("/confirm/{id}", ["as" => "confirm", "uses" => "AdminController@confirmScore"]);
            Route::get("/delete/{id}", ["as" => "delete", "uses" => "AdminController@deleteScore"]);
        });
    });
});