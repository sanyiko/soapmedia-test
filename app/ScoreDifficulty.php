<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ScoreDifficulty extends Model
{
    //

    public static function getDropdown() {
        return ScoreDifficulty::orderBy("id","ASC")->pluck("name","id");
    }

}
