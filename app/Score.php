<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Score extends Model
{
    protected $fillable = ["name", "score_difficulty_id", "score"];

    public static function getUnconfirmedScores() {
        return Score::where("approved","=","0")->orderBy("created_at","ASC")->get();
    }

    public function difficulty() {
        return $this->hasOne("\App\ScoreDifficulty","id","score_difficulty_id");
    }

    public static function confirmScore($id) {
        $score = Score::findOrFail($id);
        $score->approved = 1;
        $score->save();
    }

    public static function deleteScore($id) {
        $score = Score::findOrFail($id);
        $score->delete();
    }

    public static function getActiveScores() {
        return Score::where("approved","=","1")->orderBy("created_at","DESC")->get();
    }

}
