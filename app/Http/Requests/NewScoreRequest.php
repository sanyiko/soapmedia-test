<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NewScoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name" => "required|unique:scores,name",
            "score_difficulty_id" => "required|exists:score_difficulties,id",
            "score" => "required|numeric",
        ];
    }
}
