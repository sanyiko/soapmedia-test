<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\NewScoreRequest;
use App\Score;

class FrontendController extends Controller
{
    public function index() {
        return view("index");
    }

    public function scoreList() {
        return view("score.list");
    }

    public function scoreStore(NewScoreRequest $r) {
        $score = new Score;
        $score->create($r->all());
        return redirect()->back()->with("createScoreMsg","Your score was saved, however it will be visible only after admin approval!");
    }

}
