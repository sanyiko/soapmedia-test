<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Score;
use DB;

class ApiController extends Controller
{
    //
    public function listScores() {
        $scores = DB::table("scores")->where("approved","=","1")->join("score_difficulties","scores.score_difficulty_id","=","score_difficulties.id")
            ->select("scores.name", "score_difficulties.name AS difficulty", "scores.score")->get();
        return ["data"=>$scores->toArray()];
    }
}
