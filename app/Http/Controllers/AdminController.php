<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

use App\Score;

class AdminController extends Controller
{
    //
    public function index() {
        return view("admin");
    }

    public function logout() {
        Auth::logout();
        return redirect()->route("score.list");
    }

    public function confirmScore($id) {
        Score::confirmScore($id);
        return redirect()->back()->with("unconfirmedMsg","Score confirmed!");
    }

    public function deleteScore($id) {
        Score::deleteScore($id);
        return redirect()->back()->with("unconfirmedMsg","Score deleted!");
    }

}
