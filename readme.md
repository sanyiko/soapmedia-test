# Soapmedia UI & Backend Test

### Requirements:

* PHP >= 7.0.0
* OpenSSL PHP Extension
* PDO PHP Extension
* Mbstring PHP Extension
* Tokenizer PHP Extension
* XML PHP Extension

### Insallation:

```sh
$ git clone https://bitbucket.org/sanyiko/soapmedia-test.git
$ cd soapmedia-test
$ composer install
```

Now you have to copy or rename the ".env.example" file to ".env" in your root folder. In Windows CMD you can do this by "copy .env.example .env" or in Linux Terminal by running the "cp .env.example .env" command.

Now open the ".env" file with a text editor and set up your database credentials by changing the database name ( DB_DATABASE ), username ( DB_USERNAME ) and password ( DB_PASSWORD ).

Now run:

```sh
$ php artisan key:generate
$ php artisan migrate
$ php artisan db:seed
$ php artisan serve
```

To find my UI Test you have to open your browser and navigate to [http://localhost:8000](http://localhost:8000).

If you are curious about my Backend Test too, please navigate to [http://localhost:8000/scores](http://localhost:8000/scores).

### Optional:

To recompile my scripts and stylesheets, first you will need [Node.js](https://nodejs.org/en/) and [Gulp](https://gulpjs.com/) installed, then by running the code below:

```sh
$ npm install
$ gulp build
```