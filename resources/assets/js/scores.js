$(document).ready(function() {
    $(".tbl-score").DataTable({
        "ajax": "/api/score/list",
        "columns": [
            { "data": "name" },
            { "data": "difficulty" },
            { "data": "score" },
        ]
    });
});