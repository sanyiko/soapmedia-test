<div class="panel panel-default">
    {!! Form::open(["route"=>"score.store"]) !!}
    <div class="panel-heading">Add New Score</div>
    <div class="panel-body">
        @if($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $e)
                        <li>{{ $e }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if(Session::has("createScoreMsg"))
            <div class="alert alert-success">{{Session::get("createScoreMsg")}}</div>
        @endif
        <div class="row">
            <div class="col-md-4">
                <div class="form-group {{($errors->first("name") ? 'has-error' : '')}}">
                    {!! Form::label("name","Name") !!}
                    {!! Form::text("name",null,["class"=>"form-control"]) !!}
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group {{($errors->first("score_difficulty_id") ? 'has-error' : '')}}">
                    {!! Form::label("score_difficulty_id","Difficulty") !!}
                    {!! Form::select("score_difficulty_id",\App\ScoreDifficulty::getDropdown(),null,["class"=>"form-control"]) !!}
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group {{($errors->first("score") ? 'has-error' : '')}}">
                    {!! Form::label("score","Score") !!}
                    {!! Form::input("number","score",null,["class"=>"form-control"]) !!}
                </div>
            </div>
            <div class="col-md-12">
                {!! Form::submit("Save",["class"=>"btn btn-primary pull-right"]) !!}
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</div>