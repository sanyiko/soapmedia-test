<table class="table table-condensed table-striped table-hover">
    <thead>
        <th>Name</th>
        <th>Difficulty</th>
        <th>Score</th>
        <th class="text-right">Actions</th>
    </thead>
    <tbody>
        @forelse($scores as $s)
            <tr>
                <td>{{$s->name}}</td>
                <td>{{$s->difficulty->name}}</td>
                <td>{{$s->score}}</td>
                <td class="text-right">
                    <div class="btn-group" role="group">
                        @if($s->approved == 0)
                            <a href="{{route("confirm",$s->id)}}" class="btn btn-xs btn-success">
                                <span class="glyphicon glyphicon-ok"></span>
                            </a>
                        @endif
                        <a href="{{route("delete",$s->id)}}" class="btn btn-xs btn-danger" onclick="return confirm('Are you sure?')">
                            <span class="glyphicon glyphicon-trash"></span>
                        </a>
                    </div>
                </td>
            </tr>
        @empty
            <tr><td colspan="4" class="text-center">Empty list</td></tr>
        @endforelse
    </tbody>
</table>