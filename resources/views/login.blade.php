@extends("master")

@section("content")

    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">High Score Administration</div>
                        <div class="panel-body">
                            @if(Session::has("loginErrorMsg"))
                                <div class="alert alert-danger">{{Session::get("loginErrorMsg")}}</div>
                            @else
                                <div class="alert alert-info">
                                    Demo username: example@demo.com<br>
                                    Demo password: demo<br>
                                </div>
                            @endif
                            {!! Form::open(["route"=>"auth"]) !!}
                                <div class="form-group {{($errors->first("email") ? 'has-error' : '')}}">
                                    {!! Form::label("email","E-mail address") !!}
                                    {!! Form::email("email",null,["class"=>"form-control"]) !!}
                                </div>
                                <div class="form-group {{($errors->first("password") ? 'has-error' : '')}}">
                                    {!! Form::label("password","Password") !!}
                                    {!! Form::password("password",["class"=>"form-control"]) !!}
                                </div>
                                {!! Form::submit("Log In",["class" => "btn btn-block btn-primary"]) !!}
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection