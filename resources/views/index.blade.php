<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Soapmedia UI Test</title>
        <link href="/css/app.css" rel="stylesheet">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <nav class="navbar navbar-blue">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand navbar-brand-centered" href="{{route('index')}}">
                        <img class="logo" src="/images/logo.png" alt="Logo" />
                    </a>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><a href="#">A link</a></li>
                        <li><a href="#">Link 2</a></li>
                        <li><a href="#">Longer Link</a></li>
                        <li><a href="#">Another</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="#">Link 5</a></li>
                        <li><a href="#">Link 6</a></li>
                        <li><a href="#">Link 7</a></li>
                        <li><a class="bordered with-icon phone-icon" href="#">A button link?</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <section class="banner-heading p-t-55 p-b-65">
            <div class="container">
                <h1 class="text-center heading-primary m-t-0 m-b-50">An example of a <br> large banner heading</h1>
                <h2 class="text-center heading-secondary m-t-0 m-b-60">A Centred Header <br> on two lines</h2>
                <p class="text-center lead m-t-0">This is an example of a leading paragraph, that would go at the start of a body of text.</p>
            </div>
        </section>
        <section class="content p-t-50 p-b-60">
            <div class="container">
                <p class="colored m-t-0 m-b-30">This is a coloured sub heading</p>
                <div class="row m-b-60">
                    <div class="col-md-8">
                        <p class="heading-secondary with-colored-border p-l-20 m-b-40">A Centred Header <br> on two lines</p>
                        <p class="lead m-t-0 m-b-30">This is an example of a leading paragraph, that <br> would go at the start of a body of text.</p>
                        <ul class="blue-bullet">
                            <li>Bullet style</li>
                            <li>Bullet style</li>
                            <li>Bullet style</li>
                        </ul>
                    </div>
                    <div class="col-md-7">
                        <p class="m-t-0 m-b-40">This is a body text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean malesuada est aliquet, condimentum lorem eu, scelerisque ligula. Proin ac porttitor neque, non scelerisque lorem. Maecenas vestibulum ipsum et nisl imperdiet tincidunt. Quisque suscipit dictum diam eget pretium. Donec ac mauris eget velit vehicula facilisis. Nulla facilisi.</p>
                        <p class="m-t-0 m-b-40">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                        <a href="#" class="btn btn-blue btn-block btn-inner-border">I'm a full width button!</a>
                    </div>
                </div>
                <figure style="position: relative; height: 300px; overflow: hidden;">
                    <img class="parallax" src="/images/content-image.jpg" />
                </figure>
            </div>
        </section>

        <footer class="p-t-30 p-b-30">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <ul class="horizontal-list m-b-0">
                            <li><a href="#">Copyright</a></li>
                            <li><a href="#">Cookie Policy</a></li>
                            <li><a href="#">Terms & Conditions</a></li>
                        </ul>
                    </div>
                    <div class="col-md-7">
                        <p class="text-right footer-msg">A message about the author could go here</p>
                    </div>
                </div>
            </div>
        </footer>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="/js/main.min.js"></script>
    </body>
</html>