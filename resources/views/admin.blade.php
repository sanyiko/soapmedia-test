@extends("master")

@section("content")

    <section>
        <div class="container">
            <h1 class="heading text-center">High Score Administration</h1>
            <div class="row">
                @if(Session::has("unconfirmedMsg"))
                    <div class="col-md-12"><div class="alert alert-success">{{Session::get("unconfirmedMsg")}}</div></div>
                @endif
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">Pending Scores</div>
                        <div class="panel-body">

                            @include("includes.score-list-moderation",["scores"=>\App\Score::getUnconfirmedScores()])
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">Active Scores</div>
                        <div class="panel-body">
                            @include("includes.score-list-moderation",["scores"=>\App\Score::getActiveScores()])
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection