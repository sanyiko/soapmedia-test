@extends("master")

@section("content")
    <section>
        <div class="container">
            <h1 class="heading text-center">High Scores</h1>
            <table class="table table-striped table-condensed table-bordered tbl-score display">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Difficulty</th>
                        <th>Score</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>Name</th>
                        <th>Difficulty</th>
                        <th>Score</th>
                    </tr>
                </tfoot>
            </table>

            @include("includes.score-panel")

        </div>
    </section>
@endsection