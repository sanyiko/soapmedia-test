var gulp = require('gulp');
var sass = require('gulp-sass');
var minifyCSS = require('gulp-csso');
var browserSync = require('browser-sync').create();
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var rename = require('gulp-rename');

gulp.task('serve', ['css'], function() {
    browserSync.init({
        proxy: "http://127.0.0.1:8000"
    });
    gulp.watch("resources/assets/sass/*.scss", ['css']);
});

gulp.task('css', function(){
    return gulp.src('resources/assets/sass/app.scss')
        .pipe(sass())
        .pipe(rename('app.css'))
        .pipe(gulp.dest('public/css'))
        .pipe(minifyCSS())
        .pipe(rename('app.min.css'))
        .pipe(gulp.dest('public/css'))
        .pipe(browserSync.stream());
});

gulp.task('scripts', function() {
    return gulp.src([
            "node_modules/bootstrap-sass/assets/javascripts/bootstrap/collapse.js",
            "node_modules/scroll-parallax/dist/Parallax.js",
            "resources/assets/js/app.js",
        ])
        .pipe(concat('main.js'))
        .pipe(gulp.dest('public/js'))
        .pipe(uglify())
        .pipe(rename('main.min.js'))
        .pipe(gulp.dest('public/js'));
});

gulp.task('score_scripts', function() {
    return gulp.src([
        "node_modules/bootstrap-sass/assets/javascripts/bootstrap.js",
        "node_modules/datatables.net/js/jquery.dataTables.js",
        "node_modules/datatables.net-bs/js/dataTables.bootstrap.js",
        "resources/assets/js/scores.js",
    ])
    .pipe(concat('scores.js'))
    .pipe(uglify())
    .pipe(gulp.dest('public/js'));
});

gulp.task('score_copy_glyphicons', function(){
    return gulp.src("node_modules/bootstrap-sass/assets/fonts/bootstrap/*")
        .pipe(gulp.dest('public/fonts/bootstrap'));
});

gulp.task('score_css', function(){
    return gulp.src("resources/assets/sass/scores.scss")
        .pipe(sass())
        .pipe(minifyCSS())
        .pipe(rename('scores.css'))
        .pipe(gulp.dest('public/css'))
        .pipe(browserSync.stream());
});


gulp.task('default', ['css', 'serve', 'scripts', 'score_scripts', 'score_copy_glyphicons', 'score_css']);
gulp.task('build',['css', 'scripts', 'score_scripts', 'score_copy_glyphicons', 'score_css']);